const db = require('./db');
const logger = require('../util/logger');

function createSlot(req, res, next) {
  logger.winston.info('slots.createSlot');
  db.run('insert into slots (form_id, entity_id)' + 'values (?,?)', [req.body.form_id, req.body.entity_id], function(err) {
    if (err) {
      logger.winston.error("Error inserting a new record");
    } else {
      res.status(200).json({ status: 'success', message: 'Inserted' });
    }
  });
}

function updateSlot(req, res, next) {
  logger.winston.info('slots.updateSlot');
  db.run('update slots set entity_id = ? where slot_id = ?', [req.body.entity_id, req.body.slot_id], function(err) {
    if (err) {
      logger.winston.error("Error updating the record");
    } else {
      res.status(200).json({ status: 'success', message: 'Updated' });
    }
  });
}

function deleteSlot(req, res, next) {
  logger.winston.info('slots.deleteSlot');
  db.run('delete from slots where slot_id = ?', req.query.slot_id, function(err) {
    if (err) {
      logger.winston.error("Error removing the record");
    } else {
      res.status(200).json({ status: 'success', message: 'Removed' });
    }
  });
}

module.exports = {
  createSlot,
  updateSlot,
  deleteSlot
};