const db = require('./db');
const logger = require('../util/logger');

function getBotFormsAndSlots(req, res, next) {
  logger.winston.info('forms.getBotFormsAndSlots');
  db.all('select * from forms where bot_id = ? order by form_id desc', req.query.bot_id, function(err, forms) {
    if (err) {
      logger.winston.error(err);
    } else {
      var formIds = [];
      for (var i = 0; i < forms.length; i++) {
        formIds.push(forms[i].form_id);
      }
      if (formIds.length > 0) {
        db.all('select s.*, e.entity_name from (select * from slots where form_id in (' + formIds.splice(",") + ')) s left join entities e on e.entity_id = s.entity_id order by form_id desc', function(err, slots) {
          if (err) {
            logger.winston.error(err);
          } else {
            res.status(200).json([{forms: forms, slots: slots}]);
          }
        });
      } else {
        res.status(200).json([{forms: forms, slots: []}]);
      }
    }
  });
}

function createBotForm(req, res, next) {
  logger.winston.info('forms.createBotForm');
  db.run('insert into forms (form_name, bot_id)' + 'values (?,?)', [req.body.form_name, req.body.bot_id], function(err) {
    if (err) {
      logger.winston.error("Error inserting a new record");
    } else {
      res.status(200).json({ status: 'success', message: 'Inserted' });
    }
  });
}


function removeBotForm(req, res, next) {
  logger.winston.info('forms.removeBotForm');
  db.run('delete from forms where form_id = ?', req.query.form_id, function(err) {
    if (err) {
      logger.winston.error("Error removing the record");
    } else {
      db.run('delete from slots where form_id = ?', req.query.form_id);
      res.status(200).json({ status: 'success', message: 'Removed' });
    }
  });
}


function updateBotForm(req, res, next) {
  logger.winston.info('forms.updateBotForm');
  db.run('update forms set form_name = ? where form_id = ?', [req.body.form_name, req.body.form_id], function(err) {
    if (err) {
      logger.winston.error("Error updating the record");
    } else {
      res.status(200).json({ status: 'success', message: 'Updated' });
    }
  });
}

module.exports = {
  getBotFormsAndSlots,
  createBotForm,
  updateBotForm,
  removeBotForm
};
