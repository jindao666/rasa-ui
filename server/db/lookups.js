const db = require('./db');
const logger = require('../util/logger');

function getSingleLookup(req, res, next) {
  logger.winston.info('lookup.getSingleLookup');
  db.get('select * from lookups where lookup_id = ?', req.params.lookup_id, function(err, data) {
    if (err) {
      logger.winston.error(err);
    } else {
      res.status(200).json(data);
    }
  });
}

function getBotLookups(req, res, next) {
  logger.winston.info('lookup.getBotLookups');

  db.all('select * from lookups where bot_id = ? order by lookup_id desc', req.params.bot_id, function(err, data) {
    if (err) {
      logger.winston.error(err);
    } else {
      res.status(200).json(data);
    }
  });
}

function createBotLookup(req, res, next) {
  logger.winston.info('lookup.createBotLookup');
  db.run('insert into lookups(bot_id, entity_id)' + 'values (?,?)', [req.body.bot_id, req.body.entity_id], function(err) {
    if (err) {
      logger.winston.error("Error inserting a new record");
    } else {
      db.get('SELECT last_insert_rowid()', function(err, data) {
        if (err) {
          res.status(500).json({ status: 'error', message: '' });
        } else {
          res.status(200).json({ status: 'success', message: 'Inserted', lookup_id: data['last_insert_rowid()'] });
        }
      });
    }
  });
}

function removeLookup(req, res, next) {
  logger.winston.info('lookup.removeLookup');
  db.run("delete from lookup_tables where lookup_id = ?", req.params.lookup_id);
  db.run('delete from lookups where lookup_id = ?', req.params.lookup_id, function(err) {
    if (err) {
      logger.winston.error("Error removing the record");
    } else {
      res.status(200).json({ status: 'success', message: 'Removed' });
    }
  });
}

module.exports = {
  getSingleLookup,
  getBotLookups,
  createBotLookup,
  removeLookup
};
