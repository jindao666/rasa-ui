angular.module('app').controller('AddBotFormController', AddBotFormController);

function AddBotFormController($scope, BotForms) {
    $scope.bot_id = $scope.$routeParams.bot_id;
    $scope.addBotForm = function (params) {
        this.formData.bot_id = $scope.bot_id;
        BotForms.save(this.formData).$promise.then(function () {
            $scope.go('/responses/' + $scope.bot_id);
        });
    };
}
