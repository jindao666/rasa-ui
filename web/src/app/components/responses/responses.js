angular.module('app').controller('ResponseController', ResponseController);

function ResponseController($rootScope, $scope, $sce, Bot, Response, Actions, BotForms, Slots, BotEntities ) {
  $scope.message = {};
  $scope.message.text = "";
  $scope.bot = {};
  $scope.selectedBot = {};

  $scope.graphData = '';
  $scope.responseTypeList = [{id: "text", type: "text"}, {id: "button", type: "buttons"}];
  $scope.responseList = [];

  Bot.query(function (data) {
    $scope.botList = data;

    if ($scope.$routeParams.bot_id) {
      $scope.selectedBot = $scope.objectFindByKey($scope.botList, 'bot_id', Number($scope.$routeParams.bot_id));
      $scope.bot.bot_id = $scope.selectedBot.bot_id;
      $scope.loadBotFormsAndSlotsAndActionsAndResponses($scope.selectedBot.bot_id);
    }
  });

  // action与response操作 start
  $scope.saveResponse = function(action, action_id) {
    action.new_response.action_id = action_id;
    Response.save(action.new_response).$promise.then(function() {
      $scope.loadBotFormsAndSlotsAndActionsAndResponses($scope.selectedBot.bot_id);
    });
  };

  $scope.updateResponse = function(response_id, response_type, response_text) {
    Response.update({ response_id: response_id, response_type: response_type, response_text: response_text}).$promise.then(function() {
      $scope.go('/responses/' +  $scope.selectedBot.bot_id);
      // $rootScope.$broadcast('setAlertText', 'Response updated');
      $rootScope.$broadcast('setAlertText', '回复已更新！！！');
    });
  };

  $scope.deleteResponse = function(response_id) {
    Response.delete({response_id: response_id }, data => {
      $scope.loadBotFormsAndSlotsAndActionsAndResponses($scope.selectedBot.bot_id);
    });
  };

  $scope.deleteAction = function(action_id) {
    Actions.delete({action_id: action_id }, data => {
      $scope.loadBotFormsAndSlotsAndActionsAndResponses($scope.selectedBot.bot_id);
    });
  };
  // action与response操作 end

  // 表单与插槽操作 start
  $scope.saveFormSlot = function(form, form_id) {
    form.new_slot.form_id = form_id;
    Slots.save(form.new_slot).$promise.then(function() {
      $scope.loadBotFormsAndSlotsAndActionsAndResponses($scope.selectedBot.bot_id);
    });
  };

  $scope.updateFormSlot = function(slot_id, entity_id) {
    Slots.update({ slot_id: slot_id, entity_id: entity_id}).$promise.then(function() {
      $scope.go('/responses/' +  $scope.selectedBot.bot_id);
      // $rootScope.$broadcast('setAlertText', 'Slot updated');
      $rootScope.$broadcast('setAlertText', '插槽已更新！！！');
    });
  };

  $scope.deleteFormSlot = function(slot_id) {
    Slots.delete({slot_id: slot_id }, data => {
      $scope.loadBotFormsAndSlotsAndActionsAndResponses($scope.selectedBot.bot_id);
    });
  };
  // 表单与插槽操作 end

  $scope.deleteForm = function(form_id) {
    BotForms.delete({form_id: form_id }, data => {
      $scope.loadBotFormsAndSlotsAndActionsAndResponses($scope.selectedBot.bot_id);
    });
  };

  // 刷新页面数据
  $scope.loadBotFormsAndSlotsAndActionsAndResponses = function(bot_id) {
    $scope.selectedBot = $scope.objectFindByKey($scope.botList, 'bot_id', bot_id);

    Actions.query({bot_id: bot_id }, data => {
      $scope.actionsList = data[0].actions;
      $scope.responseList = data[0].responses;
    });

    BotForms.query({bot_id: bot_id}, data => {
      $scope.formList = data[0].forms;
      $scope.slotList = data[0].slots;
    });

    BotEntities.query({ bot_id: bot_id }, data => {
      $scope.entityList = data;
    });
  };
}
