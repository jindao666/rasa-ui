angular.module('app').controller('AddBotController', AddBotController);

function AddBotController($scope, Bot, $rootScope) {
  $scope.addBot = function(params) {
    Bot.save(this.formData).$promise.then(function() {
      $scope.formData.bot_name = "";
      $scope.go('/bots')
    }).catch(function (err) {
      if (err.data.message == 'nameExist') {
        $rootScope.$broadcast('setAlertText', "名称：" + $scope.formData.bot_name + " 已存在");
      } else if (err.data.message == 'enExist') {
        $rootScope.$broadcast('setAlertText', "英文标识：" + $scope.formData.bot_name_en + "已被占用！");
      } else if (err.data.message == 'portExist') {
        $rootScope.$broadcast('setAlertText', "端口：" + $scope.formData.port + " 已被占用！");
      } else {
        $rootScope.$broadcast('setAlertText', "新增保存失败，请稍后重试！");
      }
    });
  };
}
